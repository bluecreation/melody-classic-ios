/*

 Copyright (C) 2016 Apple Inc. All Rights Reserved.

 Abstract:
 iOS AppDelegate
 */

import UIKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
}
