//
//  MelodyViewController.swift
//  Melody
//
//  Created by Stanislav Nikolov on 27/09/2016.
//
//

import UIKit
import ExternalAccessory

protocol MelodyClassicDelegate {
    func sendData(data: Data)
}

protocol MelodyClassicProtocol {
    func didConnect(name: String)
    func didFailToConnect()
    func didDisconnect()
    func didReceiveData(data: Data)
    func setDelegate(delegate: MelodyClassicDelegate)
}

class MelodyTabBarController: UITabBarController, MelodyClassicDelegate {

    let PROTOCOL_STRING = "com.blue-creation.Melody"

    var notificationCenter: NotificationCenter {
        return NotificationCenter.default
    }

    var sessionController: EADSessionController {
        return EADSessionController.shared()
    }

    var dataTarget: MelodyClassicProtocol {
        return self.selectedViewController! as! MelodyClassicProtocol
    }

    private func registerEAAccessoryObservers() {
        notificationCenter.addObserver(self,
                                       selector: #selector(accessoryDidConnect),
                                       name: NSNotification.Name.EAAccessoryDidConnect,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(accessoryDidDisconnect),
                                       name: NSNotification.Name.EAAccessoryDidDisconnect,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(accessoryDidReceiveData),
                                       name: NSNotification.Name.EADSessionDataReceived,
                                       object: nil)
    }

    private func registerAppStateObservers() {
        notificationCenter.addObserver(self,
                                       selector: #selector(didBecomeActive),
                                       name: NSNotification.Name.UIApplicationDidBecomeActive,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(didEnterBackground),
                                       name: NSNotification.Name.UIApplicationDidEnterBackground,
                                       object: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        EAAccessoryManager.shared().registerForLocalNotifications()

        registerAppStateObservers()
        registerEAAccessoryObservers()
    }

    func didBecomeActive() {
        print("didBecomeActive");
        
        for target in self.viewControllers! {
            (target as! MelodyClassicProtocol).setDelegate(delegate: self)
        }
        
        if let accessory = EAAccessoryManager.shared().connectedAccessories.last {
            openSession(accessory: accessory)
        }
    }

    func didEnterBackground() {
        print("didEnterBackround")

        closeSession()
    }

    func openSession(accessory: EAAccessory) {
        sessionController.setupController(for: accessory, withProtocolString: PROTOCOL_STRING)

        if sessionController.openSession() {
            dataTarget.didConnect(name: accessory.name)
        } else {
            dataTarget.didFailToConnect()
        }
    }

    func closeSession() {
        print("Close session")

        sessionController.closeSession()
    }

    func accessoryDidConnect(notification: NSNotification) {
        print("accessoryDidConnect")

        guard let accessory = notification.userInfo![EAAccessoryKey] as? EAAccessory else {
            print("Unknown accessory!")
            return
        }

        openSession(accessory: accessory)
    }

    func accessoryDidDisconnect(notification: NSNotification) {
        print("accessoryDidDisconnect")

        guard notification.userInfo![EAAccessoryKey] != nil else {
            return
        }

        closeSession()

        dataTarget.didDisconnect()
    }

    func accessoryDidReceiveData(notification: NSNotification) {
        while true {
            let bytesAvailable = sessionController.readBytesAvailable()
            if bytesAvailable == 0 {
                break
            }

            guard let data = sessionController.readData(bytesAvailable) else {
                print("Couldn't read data!")
                break
            }
            
            // TODO disable
//            let text = String(data: data, encoding: String.Encoding.utf8)
//            print("Received text \(text)")

            dataTarget.didReceiveData(data: data)
        }
    }
    
    internal func sendData(data: Data) {
            // TODO disable
//        print("Sending data: \(data)")

        EADSessionController.shared().write(data)
    }
}
