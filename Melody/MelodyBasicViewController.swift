//
//  MelodyBasicViewController.swift
//  Melody
//
//  Created by Stanislav Nikolov on 27/09/2016.
//
//

import UIKit

class MelodyBasicViewController: UIViewController, MelodyClassicProtocol {
    @IBOutlet weak var tfSend: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var tfReceived: UITextField!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblVersion: UILabel!

    var delegate: MelodyClassicDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        lblVersion.text = "Melody Classic v\(version)"
    }

    func setDelegate(delegate: MelodyClassicDelegate) {
        self.delegate = delegate
    }

    func setSendEnabled(enabled: Bool) {
        tfSend.isEnabled = enabled
        btnSend.isEnabled = enabled
    }

    func didConnect(name: String) {
        lblStatus.text = "Connected to \(name)"
        setSendEnabled(enabled: true)
    }
    
    func didFailToConnect() {
        lblStatus.text = "Failed to connect"
        setSendEnabled(enabled: false)
    }
    
    func didDisconnect() {
        lblStatus.text = "Not Connected"
        setSendEnabled(enabled: false)
    }
    
    func didReceiveData(data: Data) {
        tfReceived.text = String(data: data, encoding: String.Encoding.utf8)
    }

    private func sendCurrentData() {
        let data = tfSend.text!.data(using: String.Encoding.utf8)!

        delegate!.sendData(data: data)

        tfSend.resignFirstResponder()
    }
    
    @IBAction func btnSend_TouchUpInside(_ sender: AnyObject) {
        sendCurrentData()
    }

    @IBAction func tfSend_didEndOnExit(_ sender: AnyObject) {
        sendCurrentData()
    }
}
