//
//  MelodyAdvancedViewController.swift
//  Melody
//
//  Created by Stanislav Nikolov on 27/09/2016.
//
//

import UIKit
import MessageUI

class MelodyAdvancedViewController: UIViewController, MelodyClassicProtocol, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var swEcho: UISwitch!
    @IBOutlet weak var swLogging: UISwitch!
    @IBOutlet weak var lblFileSize: UILabel!
    
    var delegate: MelodyClassicDelegate?
    var echoEnabled: Bool { return swEcho.isOn }
    var loggingEnabled: Bool { return swLogging.isOn }
    var loggedData = Data()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setDelegate(delegate: MelodyClassicDelegate) {
        self.delegate = delegate
    }

    func didConnect(name: String) {
    }

    func didFailToConnect() {
    }

    func didDisconnect() {
    }

    func didReceiveData(data: Data) {
        if echoEnabled {
            delegate?.sendData(data: data)
        }

        if loggingEnabled {
            loggedData.append(data)
            updateFileSize()
        }
    }

    func updateFileSize() {
        lblFileSize.text = "Logged Data Size: \(loggedData.count - txIndex)b"
    }

    @IBAction func btnClearFIle_TouchUpInside(_ sender: AnyObject) {
        loggedData.removeAll()
        txIndex = 0
        updateFileSize()
    }

    var txIndex = 0;
    
    @IBAction func btnSendIap_TouchUpInside(_ sender: AnyObject) {
        let MTU = 10 * 1024

        DispatchQueue.global().async {
            self.txIndex = 0

            while self.loggedData.count - self.txIndex > 0 {
                let tx = min(self.loggedData.count - self.txIndex, MTU)

                self.delegate?.sendData(data: Data(self.loggedData[self.txIndex..<tx+self.txIndex]))

                self.txIndex += tx

                DispatchQueue.main.async {
                    self.updateFileSize()
                }
            }
        }
    }

    @IBAction func btnSendEmail_TouchUpInside(_ sender: AnyObject) {
        guard MFMailComposeViewController.canSendMail() else {
            UIAlertView(title: "Couldn't send mail",
                        message: "You iDevice is not currently configured for sending email.",
                        delegate: nil,
                        cancelButtonTitle: "OK")
                .show()
            return
        }

        guard loggedData.count > 0 else {
            UIAlertView(title: "Couldn't send mail",
                        message: "No Data Saved",
                        delegate: nil,
                        cancelButtonTitle: "OK")
                .show()
            return
        }

        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self
        mailComposer.setSubject("Your Melody saved data.")
        mailComposer.setMessageBody("Here's the data you've received", isHTML: false)
        mailComposer.addAttachmentData(loggedData, mimeType: "application/octet-stream", fileName: "melody_data.bin")

        self.present(mailComposer, animated: true, completion: nil)
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
}
