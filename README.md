# Melody Classic iOS

## Overview

This is the sample app for demonstrating IAP connection with the BlueCreation's BC127 modules.
It requires a BC127 development kit and XCode 8.0+.

Please note that you need to be an MFI licencee before you can use the supplied software in production.
Please also refer to our terms and condition outlined in the LICENSE file.

## Build Instructions

1. Install the XCode programming enviroment on a macOS computer (version 8.0+ is required).
2. In the extacted archive, double click the `Melody.xcodeproj` file, opening it in XCode.
3. Connect an iPhone/iPad/iPod to the computer
3. You should now be able to compile and run the app on the connected device by clicking the `Play` icon in the top left corner in XCode.

## Usage Instructions

1. Have the app running on a connected iPhone/iPad/iPod.
2. Have a BC127 development kit in discoverable mode. If unsure, issue the `DISCOVERABLE ON` command on the kit.
3. Note down the name of the development kit, which you can find with the `GET NAME` command.
4. Open the `Settings` on the iDevice and choose `Bluetooth`. Tap on the development kit's name.
5. The iDevice will now connect to Melody using the IAP protocol, which you can confirm by the messages on the development kit's UART.
6. Once connected, you can go back to the app and start sending and receiving data.
7. You can close the connection from the development kit with `CLOSE <link_id>`.
8. You can reestablish a connection either from the phone (as in 4.) or through the `OPEN <phone_bt_address> IAP` command.
